package com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.ui;

import androidx.lifecycle.ViewModel;
import com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data.SchoolSatAPi;
import com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data.SchoolSatData;
import com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data.SchoolSatRepository;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolSatViewModel extends ViewModel {

    public SchoolSatViewModel(){ }

    private SchoolSatRepository repository = new SchoolSatRepository(
            new Retrofit.Builder()
                    .baseUrl("https://data.cityofnewyork.us")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(SchoolSatAPi.class)
    );

    Single<ArrayList<SchoolSatData>> getSchoolSatData(String schoolName) {
        return repository.getSchoolSatInfo(schoolName);
    }
}
