package com.example.a20200420_takariharrison_nycschools.feature.schoollist.data;

import java.util.ArrayList;
import io.reactivex.Single;

/*
This repository is technically a middleman code smell but it's built to scale. Plus I wanted
to show off my MVVM skills.
 */
public class SchoolListRepository {

    private SchoolListApi schoolListApi;

    public SchoolListRepository(SchoolListApi schoolListApi) {
        this.schoolListApi = schoolListApi;
    }

    public Single<ArrayList<SchoolListData>> getNycSchoolData(){
        return schoolListApi.getNycSchoolData();
    }
}
