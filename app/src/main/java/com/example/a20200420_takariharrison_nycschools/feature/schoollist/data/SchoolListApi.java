package com.example.a20200420_takariharrison_nycschools.feature.schoollist.data;

import java.util.ArrayList;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface SchoolListApi {

    @GET("https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
    Single<ArrayList<SchoolListData>> getNycSchoolData(); // returns a single since I don't expect multiple emissions
}
