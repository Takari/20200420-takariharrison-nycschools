package com.example.a20200420_takariharrison_nycschools.feature.schoollist.data;

import com.google.gson.annotations.SerializedName;

public class SchoolListData {

    @SerializedName("school_name")
    private String name;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("school_email")
    private String email;

    private String website;

    private String city;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}