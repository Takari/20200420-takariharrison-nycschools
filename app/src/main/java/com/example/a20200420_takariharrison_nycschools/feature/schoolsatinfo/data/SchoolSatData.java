package com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data;

import com.google.gson.annotations.SerializedName;

public class SchoolSatData {

    @SerializedName("sat_critical_reading_avg_score")
    private String readingScore;

    @SerializedName("sat_math_avg_score")
    private String mathScore;

    @SerializedName("sat_writing_avg_score")
    private String writingScore;

    public String getReadingScore() {
        return readingScore;
    }

    public void setReadingScore(String readingScore) {
        this.readingScore = readingScore;
    }

    public String getMathScore() {
        return mathScore;
    }

    public void setMathScore(String mathScore) {
        this.mathScore = mathScore;
    }

    public String getWritingScore() {
        return writingScore;
    }

    public void setWritingScore(String writingScore) {
        this.writingScore = writingScore;
    }

}
