package com.example.a20200420_takariharrison_nycschools.feature.schoollist.ui;

import androidx.lifecycle.ViewModel;
import com.example.a20200420_takariharrison_nycschools.feature.schoollist.data.SchoolListRepository;
import com.example.a20200420_takariharrison_nycschools.feature.schoollist.data.SchoolListApi;
import com.example.a20200420_takariharrison_nycschools.feature.schoollist.data.SchoolListData;
import java.util.ArrayList;
import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/*
If this project was larger scale I would definitely use Dagger for Dependency Injection. But Dagger
is overkill for such a small scale project (don't want to fall down that rabbit hole).
 */
public class SchoolListViewModel extends ViewModel {

    public SchoolListViewModel(){ }

    private SchoolListRepository repository = new SchoolListRepository( // In a perfect would this should be injected via constructor so that you can mock when unit testing.
            new Retrofit.Builder()
                    .baseUrl("https://data.cityofnewyork.us")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(SchoolListApi.class)
    );

    Single<ArrayList<SchoolListData>> getNycSchoolData(){
        return repository.getNycSchoolData();
    }
}
