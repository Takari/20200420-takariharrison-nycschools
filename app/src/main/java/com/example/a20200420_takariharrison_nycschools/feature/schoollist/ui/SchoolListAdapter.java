package com.example.a20200420_takariharrison_nycschools.feature.schoollist.ui;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20200420_takariharrison_nycschools.R;
import com.example.a20200420_takariharrison_nycschools.feature.schoollist.data.SchoolListData;
import com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.ui.SchoolSatActivity;

import java.util.ArrayList;

/*
In a larger application I would implement paging since the data set is huge, but I don't want to fall
down a rabbit whole.
 */
public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.ViewHolder> {

    private ArrayList<SchoolListData> schoolData;
    private Context context;

    SchoolListAdapter(ArrayList<SchoolListData> schoolData, Context context) {
        this.schoolData = schoolData;
        this.context = context;
    }

    @NonNull
    @Override
    public SchoolListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.school_list_adapter_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull SchoolListAdapter.ViewHolder holder, int position) {
        /*
        If this weren't a test project I would use String placeholders here to make the application
        translatable to multiple languages.
         */
        holder.schoolName.setText("School Name: " + schoolData.get(position).getName());
        holder.city.setText("Phone Number: " + schoolData.get(position).getCity());
        holder.phoneNumber.setText("City: " + schoolData.get(position).getPhoneNumber());
        holder.website.setText("Website: " + schoolData.get(position).getWebsite());
    }

    @Override
    public int getItemCount() {
        return schoolData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView schoolName;
        TextView city;
        TextView phoneNumber;
        TextView website;

        ViewHolder(View itemView) {
            super(itemView);
            schoolName = itemView.findViewById(R.id.schoolName);
            city = itemView.findViewById(R.id.city);
            phoneNumber = itemView.findViewById(R.id.phoneNumber);
            website = itemView.findViewById(R.id.website);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SchoolListData selectedSchoolData = schoolData.get(getAdapterPosition());

                    /*
                    Given more time I would implement a parcelable class to convert the entire class to a string, and back.
                    But I've only worked with Parables in Kotlin and don't have enough time to learn the Java syntax.
                     */
                    Intent intent = new Intent(context, SchoolSatActivity.class);
                    intent.putExtra("city", selectedSchoolData.getCity());
                    intent.putExtra("email", selectedSchoolData.getEmail());
                    intent.putExtra("name", selectedSchoolData.getName());
                    intent.putExtra("phoneNumber", selectedSchoolData.getPhoneNumber());
                    intent.putExtra("website", selectedSchoolData.getWebsite());

                    context.startActivity(intent);
                }
            });
        }
    }
}
