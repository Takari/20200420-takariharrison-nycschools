package com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.ui;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.example.a20200420_takariharrison_nycschools.R;
import com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data.SchoolSatData;
import java.util.ArrayList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class SchoolSatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);

        final TextView schoolName = findViewById(R.id.schoolName);
        schoolName.setText(getIntent().getStringExtra("name"));

        final TextView phoneNumber = findViewById(R.id.phoneNumber);
        phoneNumber.setText("Phone Number: " + getIntent().getStringExtra("phoneNumber"));

        final TextView city = findViewById(R.id.city);
        city.setText("City: " + getIntent().getStringExtra("city"));

        final TextView website = findViewById(R.id.website);
        website.setText("Website: " + getIntent().getStringExtra("website"));

        final TextView email = findViewById(R.id.email);
        email.setText("Email: " + getIntent().getStringExtra("email"));

        final TextView mathScore = findViewById(R.id.matchScore);
        final TextView readingScore = findViewById(R.id.readingScore);
        final TextView writingScore = findViewById(R.id.writingScore);


        final SchoolSatViewModel viewModel = new ViewModelProvider(this).get(SchoolSatViewModel.class);

        //Fetches the SAT scores from the selected school
        Disposable schoolDataSubscription = viewModel.getSchoolSatData(schoolName.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArrayList<SchoolSatData>>() {

                    @Override
                    public void onSuccess(ArrayList<SchoolSatData> schoolSatData) {

                        //It took me nearly 2 hours to figure out that not all schools have their SAT scores documented -_-
                        if (schoolSatData.isEmpty()){
                            mathScore.setText("SAT scores not documented (not all schools have records of their scores");
                            readingScore.setText("SAT scores not documented");
                            writingScore.setText("SAT scores not documented");
                        } else{
                            mathScore.setText("Math Score: " + schoolSatData.get(0).getMathScore());
                            readingScore.setText("Reading Score: " + schoolSatData.get(0).getReadingScore());
                            writingScore.setText("Writing Score: " + schoolSatData.get(0).getWritingScore());
                        }
                    }
                    @Override
                    public void onError(Throwable e) { Log.d("myTag", e.toString()); }
                });

    }
}
