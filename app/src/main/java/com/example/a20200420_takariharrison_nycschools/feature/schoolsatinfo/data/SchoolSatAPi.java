package com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data;

import java.util.ArrayList;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolSatAPi {

    @GET("https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
    Single<ArrayList<SchoolSatData>> getSchoolSatInfo(@Query("school_name") String schoolName);
}
