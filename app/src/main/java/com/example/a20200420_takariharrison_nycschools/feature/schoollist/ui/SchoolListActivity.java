package com.example.a20200420_takariharrison_nycschools.feature.schoollist.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.a20200420_takariharrison_nycschools.R;
import com.example.a20200420_takariharrison_nycschools.feature.schoollist.data.SchoolListData;
import java.util.ArrayList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/*
I was debating whether or not to take the single activity approach and concluded that it would take
too long and add much more complexity for little gain. So I went with two activities instead. I am kind
of bummed out that I didn't have enough time to add a progress par for some visual loading feedback.
 */
public class SchoolListActivity extends AppCompatActivity {

    private  RecyclerView nycSchoolRecyclerView;
    private SchoolListAdapter schoolListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setNavigationBarColor(Color.parseColor("#F47249")); //changes the bottom nav bar color for style

        final SchoolListViewModel viewModel = new ViewModelProvider(this).get(SchoolListViewModel.class);

        nycSchoolRecyclerView = findViewById(R.id.nycSchoolsRecyclerView);

        final Context context = this;

        /*
        Pulls data from a list of schools and populates a recycler view with the results
         */
        Disposable d = viewModel.getNycSchoolData()
                .subscribeOn(Schedulers.io()) //runs on background thread
                .observeOn(AndroidSchedulers.mainThread()) //results are pushed to the main thread
                .subscribeWith(new DisposableSingleObserver<ArrayList<SchoolListData>>() {
                    @Override
                    public void onSuccess(ArrayList<SchoolListData> schoolListData) {

                        schoolListAdapter = new SchoolListAdapter(schoolListData, context);

                        nycSchoolRecyclerView.setHasFixedSize(true);
                        nycSchoolRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        nycSchoolRecyclerView.setAdapter(schoolListAdapter);
                    }

                    @Override
                    public void onError(Throwable e) { Log.d("myTag", "onError" + e.toString()); }
                });

    }
}
