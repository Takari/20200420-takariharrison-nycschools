package com.example.a20200420_takariharrison_nycschools.feature.schoolsatinfo.data;

import java.util.ArrayList;
import io.reactivex.Single;

public class SchoolSatRepository {

    private SchoolSatAPi schoolSatAPi;

    public SchoolSatRepository(SchoolSatAPi schoolSatAPi) {
        this.schoolSatAPi = schoolSatAPi;
    }

    public Single<ArrayList<SchoolSatData>> getSchoolSatInfo(String schoolName) {
        return schoolSatAPi.getSchoolSatInfo(schoolName.toUpperCase());
    }
}
